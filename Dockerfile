FROM ruby:alpine
ENV ROUTE53_UPDATE_FREQUENCY 7200
ADD files /
RUN apk add --no-cache curl && \
    gem install bundler && bundle install

ENTRYPOINT ["ruby", "/main.rb"]

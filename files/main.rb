#!/usr/bin/env ruby

require 'aws-sdk-route53'

Dir[File.join(File.dirname(__FILE__), 'libs/*.rb')].each do |lib|
  require lib
end

validate_arguments

w = AwsWrapper.new
index_name = ENV['ROUTE53_DOMAIN_RECORD'] + '.'

loop do
  current_ip = my_ip
  puts "Current IP: '#{current_ip}'"

  begin
    records = w.fetch_a_records
    if records.key?(index_name)
      if current_ip == records[index_name][:values][0]
        puts "DNS Record #{index_name} is up-to-date. No changes needed"
      else
        puts "Updating DNS #{index_name} with new IP #{current_ip}"
        w.update_record(index_name, current_ip, 60)
      end
    else
      puts "DNS Record #{index_name} not found. Let's add it..."
      w.update_record(index_name, current_ip, 60)
    end
  rescue Aws::Route53::Errors::ServiceError => ex
    puts "AWS Error: #{ex}"
  end

  puts 'sleep ...'
  sleep ENV['ROUTE53_UPDATE_FREQUENCY'].to_i
end

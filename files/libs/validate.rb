def validate_env(varname, description)
  unless ENV.key? varname
    raise "'#{varname}' environment variable not defined!\n
      Please define it. \n#{description}"
  end
end

def validate_arguments
  validate_env('AWS_ACCESS_KEY_ID', 'aws access key')
  validate_env('AWS_SECRET_ACCESS_KEY', 'aws secret access key')
  validate_env('AWS_REGION', "Example: 'us-east-1'")
  validate_env('ROUTE53_DOMAIN_RECORD', "DNS record name. Example: 'home.mydomain.com'")
  validate_env('ROUTE53_UPDATE_FREQUENCY', 'The frequency (in seconds) to check for updates')
  puts 'Arguments validation - OK'
end

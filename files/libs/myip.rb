require 'net/http'

def my_ip
  uri = URI('http://ifconfig.co')
  req = Net::HTTP::Get.new(uri)
  req['User-Agent'] = 'curl'
  response = Net::HTTP.start(uri.hostname, uri.port) do |http|
    http.request(req)
  end
  response.body.strip!
end

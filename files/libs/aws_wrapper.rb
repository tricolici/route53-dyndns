require 'aws-sdk-route53'

class AwsWrapper
  def initialize(zone_id = nil)
    parse_domain_record
    @client = Aws::Route53::Client.new
    if zone_id.nil?
      init_zones
    else
      @zone_id = zone_id
      puts "Set zone_id to #{@zone_id}"
    end
  end

  def update_record(record_name, ip, ttl)
    resp = @client.change_resource_record_sets(change_batch: {
                                                 changes: [
                                                   {
                                                     action: 'UPSERT',
                                                     resource_record_set: {
                                                       name: record_name,
                                                       type: 'A',
                                                       ttl: ttl,
                                                       resource_records: [{ value: ip }]
                                                     }
                                                   }
                                                 ]
                                               },
                                               hosted_zone_id: @zone_id)
    p resp.to_h
  end

  def fetch_a_records
    resp = @client.list_resource_record_sets(hosted_zone_id: @zone_id,
                                             max_items: 1000)
    result = {}
    resp.resource_record_sets.each do |rec|
      next unless rec.type == 'A'
      records = []
      rec.resource_records.each do |rr|
        records << rr.value
      end
      result[rec.name] = {
        name: rec.name,
        ttl: rec.ttl,
        values: records
      }
    end
    result
  end

  private

  def parse_domain_record
    @record_name = ENV['ROUTE53_DOMAIN_RECORD'] + '.'
    puts "Record Name: '#{@record_name}'"
    arr = @record_name.split('.')
    arr.slice!(0)
    @zone_name = arr.join('.') + '.'
    puts "Zone: '#{@zone_name}'"
  end

  def init_zones
    resp = @client.list_hosted_zones(max_items: 300)
    resp.hosted_zones.each do |zone|
      next unless zone.name == @zone_name
      @zone_id = zone.id
      puts "Found zone '#{@zone_name}' id = '#{@zone_id}'"
      break
    end
  end
end

# Amazon Route53 Dynamic DNS Tool
A simple dynamic DNS service for Route53 (written in ruby).

# Supported architectures
* x86 - docker pull mtricolici/route53-dyndns:latest
* arm - docker pull mtricolici/route53-dyndns:arm

# Usage
```
docker run -dit \
  --restart always \
  --name route53-dyndns \
  -e "AWS_REGION=us-east-1" \
  -e "AWS_ACCESS_KEY_ID=somesecretkey" \
  -e "AWS_SECRET_ACCESS_KEY=somesecretkey" \
  -e "ROUTE53_DOMAIN_RECORD=home.mydomain.com" \
  -e "ROUTE53_UPDATE_FREQUENCY=7200" \
  mtricolici/route53-dyndns:arm
```

## Retrieving your external IP
curl ifconfig.co

## Required Environment Variables
* `AWS_ACCESS_KEY_ID` - An AWS Access Key
* `AWS_SECRET_ACCESS_KEY` - An AWS Secret Key
* `AWS_REGION` - The AWS region for connections
* `ROUTE53_DOMAIN_RECORD` - DNS record, for ex 'myhouse.mydomain.com'
* `ROUTE53_UPDATE_FREQUENCY` - The frequency (in seconds) to check for updates.

